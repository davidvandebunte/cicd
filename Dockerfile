FROM docker:20.10.10
RUN apk add --no-cache git py3-pip
COPY requirements.txt .
RUN pip3 install -r requirements.txt
